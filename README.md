Laboratorio de WebServices

**Elementos a utilizar**
* JavaEE, JDK 1.8
* IDE Eclipse, elegir la opción Eclipse for Java EE
* IDE Eclipse, instalar Jboss Tools (Menú: Help -> Eclipse MarketPlace. Buscar "Jboss Tools"). Este paso solo es necesario para crear un proyecto nuevo.
* Instalar Jboss tools 
* Instalar SOAP-UI
* Instalar el motor de base de datos Postgresql

**Descargar el proyecto**

Via SSH:
    
    git clone git@gitlab.com:jtrepowski/personas.git
    
Via HTTPS:
    
    git clone https://gitlab.com/jtrepowski/personas.git
    

    
CREATE SEQUENCE public.id_asignatura_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 6
  CACHE 1;
ALTER TABLE public.id_asignatura_seq
  OWNER TO postgres;

CREATE SEQUENCE public.id_persona_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 4
  CACHE 1;
ALTER TABLE public.id_persona_seq
  OWNER TO postgres;

CREATE SEQUENCE public.id_personaasignatura_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 8
  CACHE 1;
ALTER TABLE public.id_personaasignatura_seq
  OWNER TO postgres;


CREATE TABLE public.persona
(
  id integer NOT NULL DEFAULT nextval('id_persona_seq'::regclass),
  cedula integer NOT NULL,
  nombre character varying(1000),
  apellido character varying(1000),
  CONSTRAINT pk_id_persona PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.persona
  OWNER TO postgres;

CREATE TABLE public.asignatura
(
  id integer NOT NULL DEFAULT nextval('id_asignatura_seq'::regclass),
  descripcion character varying(1000),
  CONSTRAINT pk_id_asignatura PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.asignatura
  OWNER TO postgres;

CREATE TABLE public.personaasignatura
(
  id integer NOT NULL DEFAULT nextval('id_personaasignatura_seq'::regclass),
  id_persona integer NOT NULL,
  id_asignatura integer NOT NULL,
  CONSTRAINT pk_id_personaasignatura PRIMARY KEY (id),
  CONSTRAINT fk_id_asignatura FOREIGN KEY (id_asignatura)
      REFERENCES public.asignatura (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_id_persona FOREIGN KEY (id_persona)
      REFERENCES public.persona (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.personaasignatura
  OWNER TO postgres;


CREATE OR REPLACE VIEW public."tasignaturasdepersonas$" AS 
 SELECT pa.id_persona AS id,
    p.id AS id_asignatura,
    p.descripcion
   FROM personaasignatura pa
     LEFT JOIN asignatura p ON pa.id_asignatura = p.id;

ALTER TABLE public."tasignaturasdepersonas$"
  OWNER TO postgres;

CREATE OR REPLACE VIEW public."tpersonasporasignaturas$" AS 
 SELECT pa.id_asignatura AS id,
    p.id AS id_alumno,
    p.cedula,
    p.nombre,
    p.apellido
   FROM personaasignatura pa
     LEFT JOIN persona p ON pa.id_persona = p.id;

ALTER TABLE public."tpersonasporasignaturas$"
  OWNER TO postgres;
