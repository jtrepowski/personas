/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.*;
import py.una.pol.personas.service.*;

/**
 * JAX-RS Example
 * <p/>
 * Esta clase produce un servicio RESTful para leer y escribir contenido de personas
 */
@Path("/personasasignaturas")
@RequestScoped
public class PersonaAsignaturaRESTService {

    @Inject
    private Logger log;

    @Inject
    PersonaAsignaturaService personaAsignaturaService;
    
    /**
     * Creates a new member from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
     * or with a map of fields, and related errors.
     */

   
   @POST
   @Path("/asociar")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON)
   public Response asociar(PersonaAsignatura p) {
       Response.ResponseBuilder builder = null;
       System.out.println("Entro en asociar " + p.getId_persona() + " " + p.getId_asignatura());
       try {
    	   System.out.println("Entro aca 1");
    	   personaAsignaturaService.asociar(p);
    	   System.out.println("Entro aca 3");
           // Create an "ok" response
           
           //builder = Response.ok();
           builder = Response.status(201).entity("Asignatura y Persona asociadas exitosamente");
           
       } catch (SQLException e) {
           // Handle the unique constrain violation
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("bd-error", e.getLocalizedMessage());
           builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
       } catch (Exception e) {
           // Handle generic exceptions
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("error", e.getMessage());
           builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
       }

       return builder.build();
   }

   @DELETE
   @Path("/desasociar")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON)
   public Response desasociar(PersonaAsignatura p) {
	   System.out.println("Entro en desasociar " + p.getId_persona() + " " + p.getId_asignatura());
       Response.ResponseBuilder builder = null;

       try {
    	   personaAsignaturaService.desasociar(p);
           // Create an "ok" response
           
           //builder = Response.ok();
           builder = Response.status(201).entity("Asignatura y Persona desasociadas exitosamente");
           
       } catch (SQLException e) {
           // Handle the unique constrain violation
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("bd-error", e.getLocalizedMessage());
           builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
       } catch (Exception e) {
           // Handle generic exceptions
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("error", e.getMessage());
           builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
       }

       return builder.build();
   }
   
   @GET
   @Path("/personas/{idasignatura}")
   @Produces(MediaType.APPLICATION_JSON)
   public List<Persona> obtenerPersonas(@PathParam("idasignatura") long id) {
	   System.out.println("Entro en obtenerPersonas");
       List<Persona> p = personaAsignaturaService.seleccionarPersonas(id);
       if (p == null) {
       	log.info("obtenerPersonas de materia " + id + " no encontrado.");
           throw new WebApplicationException(Response.Status.NOT_FOUND);
       }
       log.info("obtenerPersonas de materia" + id + " encontrados");
       return p;
   }

   @GET
   @Path("/asignaturas/{idasignatura}")
   @Produces(MediaType.APPLICATION_JSON)
   public List<Asignatura> obtenerAsignaturas(@PathParam("idasignatura") long id) {
	   System.out.println("Entro en obtenerAsignaturas");
       List<Asignatura> p = personaAsignaturaService.seleccionarAsignaturas(id);
       if (p == null) {
       	log.info("obtenerAsignaturas de persona " + id + " no encontrado.");
           throw new WebApplicationException(Response.Status.NOT_FOUND);
       }
       log.info("obtenerAsignaturas de persona" + id + " encontrados");
       return p;
   }


}
