package py.una.pol.personas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class PersonaAsignatura  implements Serializable {

	Long id_persona;
	Long id_asignatura;
	
	
	public PersonaAsignatura(){
	}

	public PersonaAsignatura(Long id, Long id_persona, Long id_asignatura) {
		this.id_persona = id_persona;
		this.id_asignatura = id_asignatura;
		
	}

	public Long getId_persona() {
		return id_persona;
	}

	public void setId_persona(Long id_persona) {
		this.id_persona = id_persona;
	}

	public Long getId_asignatura() {
		return id_asignatura;
	}

	public void setId_asignatura(Long id_asignatura) {
		this.id_asignatura = id_asignatura;
	}	

}
