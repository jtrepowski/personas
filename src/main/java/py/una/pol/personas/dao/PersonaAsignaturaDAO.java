package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.*;

@Stateless
public class PersonaAsignaturaDAO {
 
	
    @Inject
    private Logger log;
    
	/**
	 * 
	 * @param condiciones 
	 * @return
	 */

	public List<Asignatura> seleccionarAsignaturasDePersona(long id) {
		String query = "SELECT id_asignatura, descripcion FROM tasignaturasdepersonas$ where id = ? ";
		List<Asignatura> lista = new ArrayList<Asignatura>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	pstmt.setLong(1, id);
        	ResultSet rs = pstmt.executeQuery();
        	System.out.println(query);
        	while(rs.next()) {
        		Asignatura p = new Asignatura();
        		p.setId(rs.getLong(1));
        		p.setDescripcion(rs.getString(2));
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
	}

	public List<Persona> seleccionarPersonasDeAsignatura(long id) {
		String query = "SELECT id_alumno, cedula, nombre, apellido FROM tpersonasporasignaturas$ where id = ? ";
		List<Persona> lista = new ArrayList<Persona>();
		
		Connection conn = null; 
        try {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	pstmt.setLong(1, id);
        	ResultSet rs = pstmt.executeQuery();
        	System.out.println(query);
        	while(rs.next()) {
        		Persona p = new Persona();
        		p.setCedula(rs.getLong(2));
        		p.setNombre(rs.getString(3));
        		p.setApellido(rs.getString(4));
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
	}

	public long asociar(PersonaAsignatura p) throws SQLException {

        String SQL = "INSERT INTO personaasignatura(id_persona,id_asignatura) "
                + "VALUES(?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, p.getId_persona());
            pstmt.setLong(2, p.getId_asignatura());
             int affectedRows = pstmt.executeUpdate();
            System.out.println(pstmt.toString());
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;
    	
    	
    }
	
    public long desasociar(PersonaAsignatura p) throws SQLException {

        String SQL = "DELETE FROM personaasignatura WHERE id_persona = ? and id_asignatura = ? ";
        Connection conn = null;
        long id = 0;
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1,p.getId_persona());
            pstmt.setLong(2,p.getId_asignatura());
            System.out.println(pstmt.toString()); 
            int affectedRows = pstmt.executeUpdate();

            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la desasociacion: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la desasociacion: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }
    

}
